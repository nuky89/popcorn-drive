exports.install = function(framework, name) {
	framework.on('load', function(framework){
		framework.module('scanner').scanFs();
	});
}

exports.scanFs = function(){

	var self = this;
	var scan = framework.model('options/scan');
	var extension = framework.model('options/extension');
	var movie = framework.model('movie');
	
	//clean temporally
	movie.find(function(err, results) {
		results.forEach(function(result){
			result.remove();
		})
	});

	var extList = new Array();

	extension.find({active: true}, function(err, results){
		results.forEach(function(result){
			extList.push(result.extension);
		})
	});

	scan.find(function(err, results) {
		var start = moment();
		var count = 0;
		results.forEach(function(result){
			console.log("scanning -> " + result.uri);

			var walker  = walk.walk(result.uri, {followLinks: false}).on('file', function(root, stat, next) {
				// console.log(root + '/' + stat.name);
				if(!/[a-z]:\\\/\$RECYCLE\.BIN\//i.test(root) ){ //&& stat.size > 600*1024*1024
					// console.log(root + '/' + stat.name);
					if(_.contains(extList, path.extname(stat.name).toLowerCase())){
						//console.log(root);
						//console.log(stat);
						console.log(basename = path.basename(stat.name, path.extname(stat.name)));
						basename = self.clean(basename);
						console.log(basename);
						//console.log(movie);
						new movie().set({title: basename, uri: root + path.sep + stat.name}).save();
						//ffprobe(root + path.sep + stat.name, function(err, probeData) {

							//console.log(probeData.filename.replace(/\[(([^\[\]])*)\]/gi, ""));
							//console.log(probeData);
							//console.log("time elapsed: " + start.fromNow(true));
							//console.log("movied checked: " + (++count));

						count++;	

						next();
						//});
					}else
						next();
				}else
					next();
				//
			});
			walker.on('end', function() {
				console.log("SCAN FINISHED");
				console.log("time elapsed: " + start.fromNow(true));
				console.log("movied checked: " + count);
				self.scanProbe();
			});
		});
	});
}

exports.scanProbe = function(){
	console.log("I'll probe");
	//ffprobe(root + path.sep + stat.name, function(err, probeData) {

		//console.log(probeData.filename.replace(/\[(([^\[\]])*)\]/gi, ""));
		//console.log(probeData);
		//console.log("time elapsed: " + start.fromNow(true));
		//console.log("movied checked: " + (++count));
	//});
}

exports.clean = function(basename) {
    basename = basename.replace(/\./g, " "); // . TODO: fix dotdotdot ...
	basename = basename.replace(/_/g, " "); // _
	basename = basename.replace(/ ?\[(([^\[\]])*)\] ?/g, ""); // []
	basename = basename.replace(/ ?\((([^\[\]])*)\) ?/g, ""); // ()

	basename = basename.replace(/db(.*)/gi, ""); // blu ray
	basename = basename.replace(/bluray(.*)/gi, ""); // blu ray
	basename = basename.replace(/bdisc(.*)/gi, ""); // blu ray

	basename = basename.replace(/dvd rip(.*)/gi, ""); // dvd rip

	basename = basename.replace(/[ -]by[ -](.*) ?/gi, " "); // easy author - CHECK THIS, TODO

	basename = basename.replace(/dvd(.*)/gi, ""); // dvd
	basename = basename.replace(/xvid(.*)/gi, ""); // xvid
	basename = basename.replace(/dvx(.*)/gi, ""); // dvx
	basename = basename.replace(/mpe?g(.*)/gi, ""); // mpeg
	basename = basename.replace(/x264(.*)/gi, ""); // x264
	basename = basename.replace(/h264(.*)/gi, ""); // h264
	basename = basename.replace(/ac3(.*)/gi, ""); // ac3
	basename = basename.replace(/aac(.*)/gi, ""); // aac
	basename = basename.replace(/mp[0-9](.*)/gi, ""); // mp3

	basename = basename.replace(/proper(.*)/gi, ""); // proper
	basename = basename.replace(/repack(.*)/gi, ""); // repack

	basename = basename.replace(/ ita?[ -]/gi, " "); // lang
	basename = basename.replace(/ eng?[ -]/gi, " "); // lang
	basename = basename.replace(/ spa?[ -]/gi, " "); // lang
	basename = basename.replace(/ es[ -]/gi, " "); // lang
	basename = basename.replace(/ fre?[ -]/gi, " "); // lang

	if(basename.match(/.*([0-9]|-).*?[ -](italiano?)[ -].*/gi))
		basename = basename.replace(/[ -]italiano?([^ ]*)/gi, " "); // lang
	if(basename.match(/.*([0-9]|-).*?[ -](english)[ -].*/gi))
		basename = basename.replace(/[ -]english([^ ]*)/gi, " "); // lang
	if(basename.match(/.*([0-9]|-).*?[ -](spanish)[ -].*/gi))
		basename = basename.replace(/[ -]spanish([^ ]*)/gi, " "); // lang
	if(basename.match(/.*([0-9]|-).*?[ -](french)[ -].*/gi))
		basename = basename.replace(/[ -]french([^ ]*)/gi, " "); // lang

	basename = basename.replace(/[ -]cd[0-9] ?/gi, " "); // part
	basename = basename.replace(/[ -]p[0-9] ?/gi, " "); // part
	
	basename = basename.replace(/(  )+/gi, " "); // double spaces
	
	basename = basename.replace(/-( )*-/gi, "-"); //  - - 
	basename = basename.replace(/^( )*-( )*/gi, ""); // -
	basename = basename.replace(/( )*-( )*$/gi, ""); // -

	return basename;
}

exports.allVideoExt = new Array(
	".3g2",
	".3gp",
	".asf",
	".asx",
	".avi",
	".flv",
	".m4v",
	".mov",
	".mp4",
	".mpg",
	".rm",
	".srt",
	".swf",
	".vob",
	".wmv",
	".aepx",
	".ale",
	".avp",
	".avs",
	".bdm",
	".bik",
	".bin",
	".bsf",
	".camproj",
	".cpi",
	".dash",
	".divx",
	".dmsm",
	".dream",
	".dvdmedia",
	".dvr-ms",
	".dzm",
	".dzp",
	".edl",
	".f4v",
	".fbr",
	".fcproject",
	".hdmov",
	".imovieproj",
	".ism",
	".ismv",
	".m2p",
	".mkv",
	".mod",
	".moi",
	".mpeg",
	".mts",
	".mxf",
	".ogv",
	".otrkey",
	".pds",
	".prproj",
	".psh",
	".r3d",
	".rcproject",
	".rmvb",
	".scm",
	".smil",
	".snagproj",
	".sqz",
	".stx",
	".swi",
	".tix",
	".trp",
	".ts",
	".veg",
	".vf",
	".vro",
	".webm",
	".wlmp",
	".wtv",
	".xvid",
	".yuv"
);
