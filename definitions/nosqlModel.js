var nosqlModel = function(schemaName, schema){
	var Model = function(model){
		//
		//--- private ---
		//
		var db = this.constructor.db;
		
		//
		//--- public ---
		//
		this.set = function(model){
			var _model = builders.prepare(schemaName, model);

			if(typeof _model.id  === 'undefined' && model.id){
				_model.id = model.id;
			}

			for(var key in _model)
	        	this[key] = _model[key];

			return this;
		};

		this.save = function(callback) {

			var model = this;

			var exists = true;
			/*if (model.id === 0) {
				model.id = new Date().getTime();
				exists = false;
			}*/

			if (model.id === 0 || model.id === '' || model.id === null || typeof model.id === 'undefined') {
				model.id = utils.GUID(20);
				exists = false;
			}

			var fn_callback = cleanUpCallback(callback);

			if (!exists) {
				db.insert(model, fn_callback);
			}else{
				var fn_map = function(doc) {
					if (doc.id == model.id)
						doc = model;
					return doc;
				};
				db.update(fn_map, fn_callback);
			}
			return this;
		};


		this.remove = function(callback) {
			console.log("REMOVE: " + this.id);
			this.constructor.remove({id: this.id}, callback);
		};

		if({}.toString.call(model) === '[object Object]')
			this.set(model);
	}

	//
	//--- static private ---
	//
	var dbName = schemaName;
	if(typeof arguments[2] === 'string')
		var dbName = arguments[2];

	//for results set
	var cleanUpCallback = function(callback){

		var fn_callback = utils.noop();
		if({}.toString.call(callback) === '[object Function]'){
			fn_callback = function(results) {
				var cast_results = new Array();

				results.forEach(function(result){
					cast_results.push(new Model(result));
				});

				if(callback.length === 2)
					callback(null, cast_results);
				else if(callback.length === 1)
					callback(cast_results);
			};
		}
		return fn_callback;
	}

	//for single result
	var cleanUpCallbackSingle = function(callback){

		var fn_callback = utils.noop();
		if({}.toString.call(callback) === '[object Function]'){
			fn_callback = function(result) {
				
				var cast_results = new Model(result);

				if(callback.length === 2)
					callback(null, cast_results);
				else if(callback.length === 1)
					callback(cast_results);
			};
		}
		return fn_callback;
	}

	var cleanUpFilter = function(filter){
		if({}.toString.call(filter) === '[object Number]'){
			var filter = {id: filter};
		}
		
		if({}.toString.call(filter) === '[object Object]'){
			var fn_filter = _.matches(filter);
		}else if({}.toString.call(filter) === '[object Function]'){
			var fn_filter = filter;
		}else{
			var fn_filter = function(){return true};
		}
		return fn_filter;
	}

	//
	//--- static public ---
	//
	Model.find = function(filter, callback) {
		console.log("FIND");
		if(arguments.length === 1){
			callback = arguments[0];
			filter = {};
		}

		var fn_filter = cleanUpFilter(filter);

		var fn_callback = cleanUpCallback(callback);

		this.db.all(fn_filter, fn_callback);
		return this;
	};

	Model.findById = function(id, callback) {
		var fn_filter = cleanUpFilter({id: id});
		var fn_callback = cleanUpCallbackSingle(callback);

		this.db.one(fn_filter, fn_callback);
		return this;
	}

	Model.remove = function(filter, callback) {
		//if (typeof(filter) === 'object')
			//id = filter.id;

		//var fn_filter = function(doc) {
			//return doc.id == id;
		//};

		var fn_filter = _.matches(filter);

		var fn_callback = cleanUpCallback(callback);

		this.db.remove(fn_filter, fn_callback);
		return this;
	};

	Model.schema = builders.schema(schemaName, schema);
	Model.db = framework.database(dbName);

	return Model;
}

global.nosqlModel = nosqlModel;