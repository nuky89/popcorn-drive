exports.install = function(framework) {
	framework.route('/proxy/*', view_uri, {timeout: 60000});
};

function view_uri() {
	var start = moment();
	var self = this;

	var uri = self.uri.path.replace(/^\/proxy\//, '');

	var data = '';
	self.async.await(function(next) {
		utils.download(uri, function(err, response) {
			//console.log("CB START - time elapsed: " + moment().diff(start));
		    response.on('data', function(buffer) {
		        data += buffer.toString('utf8');
		    });

		    response.on('end', function() {
		        next();
		        //console.log("END - time elapsed: " + moment().diff(start));
		    });

		});
	});

	var proxy_url = uri.replace(uri.match(/[^\/]\/([^\/]+.*)/)[1], ''); 
	var href = 'href="' + self.uri.protocol + '//' + self.uri.host + '/proxy/' + proxy_url;
	//console.log("SETUP - time elapsed: " + moment().diff(start));
	self.async.complete(function() {
		framework.responseContent(self.req, self.res, 200, data.replace(/href="(\/)/gi, href), 'text/html', true);
		//console.log("RESPONSE - time elapsed: " + moment().diff(start));
	});
};