exports.install = function(framework) {
	framework.route('/options/extension/', json_list);
	//framework.route('/options/extension/{id}/', json_get);
	framework.route('/options/extension/{id}/', json_update, ['post', 'json']);
	//framework.route('/options/extension/', json_create, ['post', 'json']);
	//framework.route('/options/extension/{id}/', json_delete, ['delete']);
	framework.on('load', function(framework){
		init(framework);
	});
}

function init(framework){
	var self = framework.controller('options/extension');
	var model = framework.model('options/extension');
	model.find(function(err, docs) {
		if(docs.length === 0){
			framework.module('scanner').allVideoExt.forEach(function(ext){
				new model({extension: ext, active: true}).save();
			});
		}
	});
}

/*
	Description: Get
	Method: GET
	Output: JSON
*/
function json_list() {
	var self = this;
	var model = self.model('options/extension');

	console.log('query -> all');

	model.find(function(err, docs) {
		self.json(docs);
	});
}

/*
	Description: Get
	Method: GET
	Output: JSON
*/
function json_get(id) {

	var self = this;
	var model = self.model('options/extension');

	console.log('get ->', id);

	model.findById(id, function(err, doc) {
		self.json(doc);
	});

}

/*
	Description: Save
	Method: POST
	Output: JSON
*/
function json_update(id) {

	var self = this;
	var model = self.model('options/extension');

	console.log('save ->', id);

	// What is it? https://github.com/totaljs/examples/tree/master/changes
	self.change('scan: save, id: ' + id);
	model.findById(id, function(err, doc) {
		if(err != "")
			console.log("ERROR:"+ err);
		doc.set(self.req.data.post).save();
		self.json({ r: true });
	});

}

/*
	Description: Create
	Method: POST
	Output: JSON
*/
function json_create() {
	var self = this;
	var model = self.model('options/extension');

	var doc = new model();
	doc.set(self.req.data.post).save();
	self.json({ r: true, id: doc.id });
}

/*
	Description: Delete
	Method: DELETE
	Output: JSON
*/
function json_delete(id) {

	var self = this;
	var model = self.model('options/extension');

	console.log('delete ->', id);

	// What is it? https://github.com/totaljs/examples/tree/master/changes
	self.change('deleted, id: ' + id);

	model.findById(id, function(err, doc) {
		// Please do not remove a document (THANKS :-))
		doc.remove();
		self.json({ r: true });
	});

}