exports.install = function(framework) {
	framework.route('/movie/', json_movie_query);
	framework.route('/movie/{id}/', json_movie_get, {timeout: false});
	framework.route('/movie/{id}/', json_movie_update, ['post', 'json']);
	framework.route('/movie/{id}/', json_movie_delete, ['delete']);
};

/*
	Description: Get movies
	Method: GET
	Output: JSON
*/
function json_movie_query() {
	var self = this;
	var movie = self.model('movie');

	console.log('query -> all');

	movie.find(function(err, docs) {
		self.json(docs);
	});
}

/*
	Description: Get movie
	Method: GET
	Output: JSON
*/
function json_movie_get(id) {

	var self = this;
	var movie = self.model('movie');

	console.log('get ->', id);

	movie.findById(id, function(err, doc) {
		self.json(doc);
	});

}

/*
	Description: Save movie
	Method: POST
	Output: JSON
*/
function json_movie_update(id) {

	var self = this;
	var movie = self.model('movie');

	console.log('save ->', id);

	// What is it? https://github.com/totaljs/examples/tree/master/changes
	self.change('movie: save, id: ' + id);
	movie.findById(id, function(err, doc) {
		if(err != "")
			console.log("ERROR:"+ err);
		doc.set(self.req.data.post).save();
		self.json({ r: true });
	});
}

/*
	Description: Delete movie
	Method: DELETE
	Output: JSON
*/
function json_movie_delete(id) {

	var self = this;
	var movie = self.model('movie');

	console.log('delete ->', id);

	// What is it? https://github.com/totaljs/examples/tree/master/changes
	self.change('movie: deleted, id: ' + id);

	movie.findById(id, function(err, doc) {
		doc.remove();
		self.json({ r: true });
	});

}