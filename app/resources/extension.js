app.factory('Extension', function($resource) {
	return $resource('/options/extension/:id', { id: '@id' });
});