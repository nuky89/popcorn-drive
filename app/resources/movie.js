app.factory('Movie', function($resource) {
	return $resource('/movie/:id', { id: '@id' });
});