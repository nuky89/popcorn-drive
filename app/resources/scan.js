app.factory('Scan', function($resource) {
	return $resource('/options/scan/:id', { id: '@id' });
});