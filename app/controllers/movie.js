function MovieCtrl($scope, Movie, $modal) {

	$scope.movies = Movie.query();
	$scope.isForm = false;

	$scope.edit = function(id) {   
		$scope.movie = Movie.get({ id: id });
		$scope.isForm = true;
	};

	$scope.cancel = function() {
		$scope.isForm = false;
	};

	$scope.update = function() {
		$scope.movie.$save({ id: $scope.movie.id }, function(){
			$scope.movies = Movie.query();
		});
		$scope.isForm = false;
	};

	$scope.delete = function(id) {

		Movie.delete({ id: id }, function() {
			// Refresh movies
			// $scope.movies = Movie.query();
			alert('Movie was removed.');
		});

		$scope.isForm = false;
	};

	$scope.wikipediaModal = function(movieName){
		/*
		// Needed if you embed wikipedia home page 
		window.onbeforeunload = function() {
			window.onbeforeunload = null;
			return "Wikipedia cerca di chiudere la pagina automaticamente!";
		};
		*/ 
		var modalInstance = $modal.open({
			templateUrl: '/templates/modalIframe.html',
			controller: ModalIframeCtrl,
			resolve: {
				url: function(){
					return "http://it.wikipedia.org/w/index.php?search="+movieName;
				}
			}
		});
	};

	$scope.wikipediaExt = function(movieName){
		window.open("http://it.wikipedia.org/w/index.php?search="+movieName);
	};

	$scope.imdbModal = function(movieName){
		var modalInstance = $modal.open({
			templateUrl: '/templates/modalIframe.html',
			controller: ModalIframeCtrl,
			resolve: {
				url: function(){
					return '/proxy/http://www.imdb.com/find?s=tt&q=' + movieName;
				}
			}
		});
	};

	$scope.imdbExt = function(movieName){
		window.open("http://www.imdb.com/find?s=tt&q="+movieName);
	};
}
