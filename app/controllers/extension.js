function ExtensionCtrl($scope, Extension, $modal) {

	$scope.extensions = Extension.query();

	$scope.modal = function(){
		var modalInstance = $modal.open({
			templateUrl: '/templates/modalExtension.html',
			controller: ModalExtensionCtrl,
			resolve: {
				extensions: function(){
					return $scope.extensions;
				}
			}
		});

		modalInstance.result.then(function (extensions) {
			console.log("close");
			$scope.extensions = extensions;
			$scope.updateAll();
		});
	};

	$scope.updateAll = function() {
		$scope.extensions.forEach(function(extension){
			extension.$save();
		})
	};
}

var ModalExtensionCtrl = function($scope, $modalInstance, extensions, Extension) {
	$scope.extensions = extensions.slice(0);

	$scope.ok = function() {
		$modalInstance.close($scope.extensions);
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};

};
