var ModalIframeCtrl = function($scope, $modalInstance, $sce, url) {
	
	$scope.name = name;

	$scope.url = $sce.trustAsResourceUrl(url);

	$scope.isLoading = false;

	$scope.$watch('name', function(newValue, oldValue) {
 		$scope.isLoading = true;
	});

	$scope.ok = function() {
		$modalInstance.close();
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};

	window.modalIframeLoaded = function(){
		if($scope.isLoading){
			$scope.isLoading = false;
			$scope.$apply();
		}
	}
};