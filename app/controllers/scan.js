function ScanCtrl($scope, Scan, Shared) {

    $scope.scans = Scan.query();
    $scope.scan = {};
    $scope.Shared = Shared;

    $scope.update = function(scan) {
        scan.$save({ id: scan.id }, function(){
            $scope.scans = Scan.query();
        });
    };

    $scope.create = function(){
		$scope.update(new Scan($scope.scan));
		$scope.scan = {};
    };

    $scope.delete = function(id) {
		Scan.delete({ id: id }, function() {
			$scope.scans = Scan.query();
		});
    };
}